﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Security;
using System.Web;
using Microsoft.SharePoint.Client;

namespace CSFApp
{
    class Program
    {
        private static ClientContext context = null;
        static void Main(string[] args)
        {
            //Console.WriteLine(GetMaxExcelIDFromSP("All content from submission forms").ToString());
            //string filePath = @"https://automationanywhere1-my.sharepoint.com/personal/amit_mathur_automationanywhere_com/_layouts/15/Doc.aspx?sourcedoc=%7B330A636A-DF2B-48C7-8E40-CFFD127CBB5D%7D&file=Discovery%20Workshop%20Questions_William%20Morgan.docx&action=default&mobileredirect=true";
            //string filePath = @"https://automationanywhere1-my.sharepoint.com/personal/amit_mathur_automationanywhere_com/Documents/Apps/Microsoft%20Forms/Global%20Information%20Gathering%20Form%20Data/Please%20upload%20any%20relevant%20documents%20that%20you%20woul/Claims%20Processing%20Role%20Play%20Contact%20Centre%20v2_William%20Morgan.mp4";
            //Console.WriteLine(GetFileNamesFromFilePath(filePath)[0]);


            var csfExcelFilePath = @"C:\Users\pinkesh.achhodwala\OneDrive - Automation Anywhere Software Private Limited\Sales POC\ContentSubmissionForms\Global content submission form for Sales Engineering __(1-98).xlsx";
            var attachmentFileBasePath = @"C:\Users\pinkesh.achhodwala\Automation Anywhere Software Private Limited\Amit Mathur - Please upload any relevant documents that you woul";
            //var sharePointUrl = "https://automationanywhere1.sharepoint.com/sites/TestSiteCSF";
            var sharePointUrl = "https://automationanywhere1.sharepoint.com/sites/SSSResources";
            var userName = "pinkesh.achhodwala@automationanywhere.com";
            var password = "";
            var docLibraryName = "All content";

            //Upload the files and data from excel to SharePoint site.
            //var outputMessage = UploadCSFDataToSPsite(csfExcelFilePath, attachmentFileBasePath, sharePointUrl, userName, password, docLibraryName);

            //Upload the Split PPT files.
            string splitFileDataExcelFilePath = @"C:\Users\\pinkesh.achhodwala\OneDrive - Automation Anywhere Software Private Limited\Sales POC\A360-PPTX\output.xls";
            string pptSplitFilesBasePath = @"C:\Users\pinkesh.achhodwala\OneDrive - Automation Anywhere Software Private Limited\Sales POC\A360-PPTX\splitFiles";

            var outputMessage = UploadPptSlidesDataToSPsite(splitFileDataExcelFilePath, pptSplitFilesBasePath, sharePointUrl, userName, password, docLibraryName);

            Console.WriteLine(outputMessage);
            Console.ReadLine();
        }

        public static string UploadCSFDataToSPsite(string csfExcelFilePath, string attachmentFileBasePath, string SPUrl, string userName, string password, string documentLibraryName)
        {
            //Set the Client Context (global variable).
            var outputMessage = SetClientContext(SPUrl, userName, password);
            if (!string.IsNullOrEmpty(outputMessage))
                return outputMessage;

            //Get Max ExcelID field value from SP.
            outputMessage = GetMaxExcelIDFromSP(documentLibraryName);
            if (!int.TryParse(outputMessage, out int maxSPExcelID))
                return outputMessage;

            //return "Err is: " + GetExcelDataTable(csfExcelFilePath, "SELECT * from [Sheet1$] where [ID] > " + maxSPExcelID + " order by [ID] asc");

            //Get the row from excel with ID > maxSPExcelID and start processing that row and subsequent rows.
            var excelDt = GetExcelDataTable(csfExcelFilePath, "SELECT * from [Sheet1$] where [ID] > " + maxSPExcelID + " order by [ID] asc");
            if (excelDt == null)
                return "Error occured while getting excel data in data table";

            //Start processing each rows from the Excel data and upload it on SharePoint site.
            foreach (DataRow currRow in excelDt.Rows)
            {
                var rowIDExists = int.TryParse(currRow["ID"].ToString(), out int excelID);
                var attachmentColValue = currRow["Please upload any relevant documents that you would like to shar"];
                var attachmentFilePathExists = attachmentColValue != null && !string.IsNullOrEmpty(attachmentColValue.ToString().Trim());

                //If attachment file path exists, then upload file and corresponding data to SharePoint site.
                if (rowIDExists && attachmentFilePathExists)
                {
                    var attachmentFileNameList = GetFileNamesFromFilePath(attachmentColValue.ToString().Trim());
                    foreach (string attachmentFileName in attachmentFileNameList)
                        UploadFileAndColumnValuesToSharePoint(documentLibraryName, attachmentFileBasePath + @"\" + attachmentFileName, currRow);
                }
                else //No attachment path found in master excel data.
                {
                    //log message: "Attachment file path is blank or empty in the Source excel file at row ID: #" + excelID;
                }
            }

            return "Sucessfully uploaded " + excelDt.Rows.Count + " record(s) to SharePoint document library: " + documentLibraryName;
        }

        public static string SetClientContext(string SPUrl, string userName, string password)
        {
            try
            {
                // Instantiate the secure string.
                var securePwd = new SecureString();
                char[] charArr = password.ToCharArray();
                foreach (char ch in charArr)
                    securePwd.AppendChar(ch);

                context = new ClientContext(SPUrl)
                {
                    Credentials = new SharePointOnlineCredentials(userName, securePwd)
                };

                return string.Empty;
            }
            catch (Exception ex)
            {
                return "Error occured while setting the client context: " + ex.Message;
            }
        }

        public static List<string> GetFileNamesFromFilePath(string fullFilePath)
        {
            var files = new List<string>();
            fullFilePath = HttpUtility.UrlDecode(fullFilePath);

            try
            {
                string[] spearator = { "https:" };
                string[] filePaths = fullFilePath.Split(spearator, 10, StringSplitOptions.RemoveEmptyEntries);

                foreach (var currfilePath in filePaths)
                {
                    var filePath = "https:" + currfilePath;
                    string fileName;

                    if (filePath.Contains(@"&file="))
                    {
                        int startIndex = filePath.IndexOf(@"&file=");
                        int endIndex = filePath.IndexOf(@"&action=");
                        fileName = filePath.Substring(startIndex, endIndex - startIndex).Replace("&file=", string.Empty);
                        files.Add(fileName.Replace(";", ""));
                    }
                    else if (filePath.Contains(@"woul/"))
                    {
                        int startIndex = filePath.IndexOf(@"woul/");
                        fileName = filePath.Substring(startIndex, (filePath.Length) - startIndex).Replace(@"woul/", string.Empty);
                        files.Add(fileName.Replace(";", ""));
                    }
                }

                return files;
            }
            catch (Exception)
            {
                return files;
            }

        }

        public static string GetMaxExcelIDFromSP(string documentLibraryName)
        {
            try
            {
                //Get Max ExcelID field value from SP
                var library = context.Web.Lists.GetByTitle(documentLibraryName);
                var camlQuery = new CamlQuery();
                var collListItem = library.GetItems(camlQuery);
                context.Load(collListItem);
                context.ExecuteQuery();

                int maxExcelRefID = 0;
                foreach (var field in collListItem)
                {
                    var excelRefID = field["ExcelRefID"];
                    if (excelRefID != null)
                    {
                        if (int.TryParse(excelRefID.ToString(), out int currFieldExcelID) && currFieldExcelID > maxExcelRefID)
                            maxExcelRefID = currFieldExcelID;
                    }
                }

                return maxExcelRefID.ToString();
            }
            catch (Exception ex)
            {
                return "Error occured while getting max excelID from SharePoint: " + ex.Message;
            }
        }

        public static DataTable GetExcelDataTable(string excelFilePath, string sql)
        {
            var connString = string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties='Excel 12.0;HDR=yes'", excelFilePath);
            DataTable dt = null;

            try
            {
                using (var conn = new OleDbConnection(connString))
                {
                    conn.Open();
                    using (var cmd = new OleDbCommand(sql, conn))
                    {
                        using (var reader = cmd.ExecuteReader())
                        {
                            dt = new DataTable();
                            dt.Load(reader);
                        }
                    }
                }

                return dt;
                //return string.Empty;
            }
            catch (Exception ex)
            {
                return dt;
                //return "Excep is: " + ex.Message;
            }
        }

        public static string TestExcelDataTable(string excelFilePath, string sheetName)
        {
            var connString = string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties='Excel 12.0;HDR=yes'", excelFilePath);
            DataTable dt = null;

            try
            {
                using (var conn = new OleDbConnection(connString))
                {
                    conn.Open();
                    using (var cmd = new OleDbCommand("SELECT * from [" + sheetName + "$]", conn))
                    {
                        using (var reader = cmd.ExecuteReader())
                        {
                            dt = new DataTable();
                            dt.Load(reader);
                        }
                    }
                }

                //return dt;
                return "Successful excel connection with row count: " + dt.Rows.Count;
            }
            catch (Exception ex)
            {
                //return dt;
                return "Exception Message: " + ex.Message + Environment.NewLine + " Exception details: " + ex.InnerException.ToString();
            }
        }

        private static string UploadFileAndColumnValuesToSharePoint(string docLibrary, string attachmentFullFilePath, DataRow excelDataRow)
        {
            try
            {
                #region Insert the data
                var web = context.Web;
                var documentLibrary = web.Lists.GetByTitle(docLibrary);
                var fileInfo = new FileInfo(attachmentFullFilePath);

                //If attachment is available, then Upload file and related column values on SP.
                if (fileInfo.Exists)
                {
                    var newFile = new FileCreationInformation();
                    byte[] FileContent = System.IO.File.ReadAllBytes(attachmentFullFilePath);
                    newFile.ContentStream = new MemoryStream(FileContent);
                    newFile.Url = Path.GetFileName(attachmentFullFilePath);

                    var clientfolder = documentLibrary.RootFolder;
                    Microsoft.SharePoint.Client.File uploadFile = clientfolder.Files.Add(newFile);
                    clientfolder.Update();
                    context.ExecuteQuery();

                    //Load the library column data.
                    context.Load(documentLibrary);
                    context.Load(uploadFile);
                    context.ExecuteQuery();

                    //Load field (column) values.
                    var field = uploadFile.ListItemAllFields;
                    context.Load(field);
                    context.ExecuteQuery();

                    //Start updating the required SP column values.
                    FieldUserValue userValue = new FieldUserValue();
                    User user = context.Web.EnsureUser(excelDataRow["Email"].ToString());
                    context.Load(user);
                    context.ExecuteQuery();

                    userValue = new FieldUserValue
                    {
                        LookupId = user.Id
                    };

                    field["ExcelRefID"] = excelDataRow["ID"];
                    field["SubmittersName"] = userValue;
                    field["Region"] = excelDataRow["Region"].ToString().Trim().Split(";".ToCharArray());
                    field["Country"] = excelDataRow["Country:"].ToString().Trim();
                    field["Contenttype0"] = excelDataRow["What content are you submitting?"].ToString().Trim().Split(";".ToCharArray());
                    field["Contentsanitizationstatus"] = excelDataRow["Is the content sanitized?"].ToString().Trim();
                    field["CompanyName"] = excelDataRow["Company Name:"];
                    field["CustomerIndustry"] = excelDataRow["Customer Industry:"].ToString().Trim().Split(";".ToCharArray());
                    field["UseCase"] = excelDataRow["What was the use case?"];
                    field["Solutionprovided"] = excelDataRow["What was the solution provided?"];
                    field["Benefits"] = excelDataRow["What were the benefits?"];
                    field["Businessfunction"] = excelDataRow["Business functions automated:"];
                    field["Processautomated"] = excelDataRow["Processes automated:"];
                    field["ProductDeployed"] = excelDataRow["Products Deployed:"].ToString().Trim().Split(";".ToCharArray());
                    field["Additionalcomment"] = excelDataRow["Any additional information:"];

                    field.Update();
                    context.ExecuteQuery();

                    return string.Empty;
                }
                else
                    return "Attachment file does not exists at path: " + attachmentFullFilePath;
                #endregion
            }
            catch (Exception ex)
            {
                return "Error occured while uploading file to SP: " + ex.ToString();
            }
        }

        public static string UploadPptSlidesDataToSPsite(string splitFileDataExcelFilePath, string pptSplitFilesBasePath, string SPUrl, string userName, string password, string documentLibraryName)
        {
            //Set the Client Context (global variable).
            var outputMessage = SetClientContext(SPUrl, userName, password);
            if (!string.IsNullOrEmpty(outputMessage))
                return outputMessage;

            ////Get Max ExcelID field value from SP.
            //outputMessage = GetMaxExcelIDFromSP(documentLibraryName);
            //if (!int.TryParse(outputMessage, out int maxSPExcelID))
            //    return outputMessage;

            //return "Err is: " + GetExcelDataTable(csfExcelFilePath, "SELECT * from [Sheet1$] where [ID] > " + maxSPExcelID + " order by [ID] asc");

            //Get the row from excel with ID > maxSPExcelID and start processing that row and subsequent rows.
            var excelDt = GetExcelDataTable(splitFileDataExcelFilePath, "SELECT * from [Sheet1$] order by [Slide Number] asc");
            if (excelDt == null)
                return "Error occured while getting excel data in data table";

            //Start processing each rows from the Excel data and upload it on SharePoint site.
            foreach (DataRow currRow in excelDt.Rows)
            {
                //var rowIDExists = int.TryParse(currRow["ID"].ToString(), out int excelID);
                var splitFileName = currRow["Split filename"];
                var splitFilenameExists = splitFileName != null && !string.IsNullOrEmpty(splitFileName.ToString().Trim());

                //If attachment file path exists, then upload file and corresponding data to SharePoint site.
                if (splitFilenameExists)
                {
                    UploadSplitFileAndColumnValuesToSharePoint(documentLibraryName, pptSplitFilesBasePath + @"\" + splitFileName, currRow);
                }
                else //No attachment path found in master excel data.
                {
                    //log message: "Attachment file path is blank or empty in the Source excel file at row ID: #" + excelID;
                }
            }

            return "Sucessfully uploaded " + excelDt.Rows.Count + " record(s) to SharePoint document library: " + documentLibraryName;
        }

        private static string UploadSplitFileAndColumnValuesToSharePoint(string docLibrary, string attachmentFullFilePath, DataRow excelDataRow)
        {
            try
            {
                #region Insert the data
                var web = context.Web;
                var documentLibrary = web.Lists.GetByTitle(docLibrary);
                var fileInfo = new FileInfo(attachmentFullFilePath);

                //If attachment is available, then Upload file and related column values on SP.
                if (fileInfo.Exists)
                {
                    var newFile = new FileCreationInformation();
                    byte[] FileContent = System.IO.File.ReadAllBytes(attachmentFullFilePath);
                    newFile.ContentStream = new MemoryStream(FileContent);
                    newFile.Url = Path.GetFileName(attachmentFullFilePath);

                    var clientfolder = documentLibrary.RootFolder;
                    Microsoft.SharePoint.Client.File uploadFile = clientfolder.Files.Add(newFile);
                    clientfolder.Update();
                    context.ExecuteQuery();

                    //Load the library column data.
                    context.Load(documentLibrary);
                    context.Load(uploadFile);
                    context.ExecuteQuery();

                    //Load field (column) values.
                    var field = uploadFile.ListItemAllFields;
                    context.Load(field);
                    context.ExecuteQuery();

                    //Start updating the required SP column values.
                    FieldUserValue userValue = new FieldUserValue();
                    User user = context.Web.EnsureUser(excelDataRow["Submitter"].ToString());
                    context.Load(user);
                    context.ExecuteQuery();

                    userValue = new FieldUserValue
                    {
                        LookupId = user.Id
                    };

                    //field["ExcelRefID"] = excelDataRow["ID"];
                    field["SubmittersName"] = userValue;
                    field["Region"] = excelDataRow["Region"].ToString().Trim().Split(";".ToCharArray());
                    field["Country"] = excelDataRow["Country"].ToString().Trim();
                    field["Contenttype0"] = excelDataRow["Content type"].ToString().Trim().Split(";".ToCharArray());
                    field["Contentsanitizationstatus"] = excelDataRow["Content sanitization status"].ToString().Trim();
                    field["CompanyName"] = excelDataRow["Company Name"];
                    field["CustomerIndustry"] = excelDataRow["Industry"].ToString().Trim().Split(";".ToCharArray());
                    //field["UseCase"] = excelDataRow["What was the use case?"];
                    //field["Solutionprovided"] = excelDataRow["What was the solution provided?"];
                    //field["Benefits"] = excelDataRow["What were the benefits?"];
                    //field["Businessfunction"] = excelDataRow["Business functions automated:"];
                    //field["Processautomated"] = excelDataRow["Processes automated:"];
                    //field["ProductDeployed"] = excelDataRow["Products Deployed:"].ToString().Trim().Split(";".ToCharArray());
                    //field["Additionalcomment"] = excelDataRow["Any additional information:"];

                    field.Update();
                    context.ExecuteQuery();

                    return string.Empty;
                }
                else
                    return "Attachment file does not exists at path: " + attachmentFullFilePath;
                #endregion
            }
            catch (Exception ex)
            {
                return "Error occured while uploading file to SP: " + ex.ToString();
            }
        }


    }
}
